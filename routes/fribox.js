var express = require('express');
var router = express.Router();

/* GET fribox page. */
router.get('/', function(req, res, next) {
  res.render('fribox');
});

module.exports = router;
